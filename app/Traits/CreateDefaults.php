<?php

namespace App\Traits;

use Carbon\Carbon;

trait CreateDefaults
{
    public static function getItemsFromStorage($filename)
    {
        $ASSOCIATIVE = true;
        $path = storage_path() . "/data/$filename.json";
        return json_decode(file_get_contents($path), $ASSOCIATIVE);
    }

    public static function addUserId($rows, $userId)
    {
        $rowsWithUserId = [];
        foreach ($rows as $row) {
            $row['user_id'] = $userId;
            $rowsWithUserId[] = $row;
        }
        return $rowsWithUserId;
    }

    public static function addTimestamps($rows)
    {
        $rowsWithTimestamps = [];
        foreach ($rows as $row) {
            $row['created_at'] = Carbon::now();
            $row['updated_at'] = Carbon::now();
            $rowsWithTimestamps[] = $row;
        }
        return $rowsWithTimestamps;
    }

    public static function getItems($filename, $userId)
    {
        $items = CreateDefaults::getItemsFromStorage($filename);
        $items = CreateDefaults::addUserId($items, $userId);
        return CreateDefaults::addTimestamps($items);
    }

    private static function replaceDates($rowToSearch)
    {
        $nextYear = Carbon::now()->year + 1;
        $currentYear = Carbon::now()->year;
        $currentDate = Carbon::now()->format('Y-m-d');

        $search = ['{next-year}', '{current-year}', '{current-date}'];
        $replace = [$nextYear, $currentYear, $currentDate];

        return str_replace($search, $replace, $rowToSearch);
    }

    public static function updateDates($rows)
    {
        $rowsWithUpdatedDates = [];
        foreach ($rows as $row) {
            $row['title'] = CreateDefaults::replaceDates($row['title']);
            $row['start_date'] = CreateDefaults::replaceDates($row['start_date']);
            $row['end_date'] = CreateDefaults::replaceDates($row['end_date']);
            $rowsWithUpdatedDates[] = $row;
        }
        return $rowsWithUpdatedDates;
    }
}
