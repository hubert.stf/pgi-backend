<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'language'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function paymentMethods()
    {
        return $this->hasMany(PaymentMethod::class);
    }

    public function paymentRecurrences()
    {
        return $this->hasMany(PaymentRecurrence::class);
    }

    public function paymentStatuses()
    {
        return $this->hasMany(PaymentStatus::class);
    }

    public function periods()
    {
        return $this->hasMany(Period::class);
    }

    public static function populateDatabaseWithExampleValues ($userID)
    {
        Category::createDefaultCategories($userID);
        PaymentMethod::createDefaultPaymentMethods($userID);
        PaymentRecurrence::createDefaultPaymentRecurrences($userID);
        PaymentStatus::createDefaultPaymentStatuses($userID);
        Period::createDefaultPeriods($userID);
    }
}
