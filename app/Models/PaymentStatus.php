<?php

namespace App\Models;

use App\Traits\CreateDefaults;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentStatus extends Model
{
    use HasFactory, CreateDefaults, SoftDeletes;

    protected $fillable = [
        'user_id',
        'title',
        'color'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public static function getPaymentStatusesWithExpensesCount($userID)
    {
        $user = User::find($userID);
        return $user->paymentStatuses()->withCount('expenses')->orderBy('title')->get();
    }

    public static function createDefaultPaymentStatuses($userID)
    {
        $paymentStatuses = CreateDefaults::getItems('payment-statuses-example', $userID);
        return PaymentStatus::insert($paymentStatuses);
    }
}
