<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenseAmountDiscount extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'expense_amount_id',
        'description',
        'amount'
    ];

    public function expenseAmount()
    {
        return $this->belongsTo(ExpenseAmount::class);
    }
}
