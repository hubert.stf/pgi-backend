<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncomeAmount extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'income_id',
        'user_id',
        'reference_month',
        'due_date',
        'amount',
        'payment_received_at',
        'is_payment_confirmed'
    ];

    public function income()
    {
        return $this->belongsTo(Income::class);
    }

    public static function getAllIncomeAmountsByIncomeId($incomeId)
    {
        return IncomeAmount::where('income_id', '=', $incomeId)
            ->orderBy('reference_month')
            ->orderBy('due_date')
            ->get();
    }
}
