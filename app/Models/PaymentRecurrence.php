<?php

namespace App\Models;

use App\Traits\CreateDefaults;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentRecurrence extends Model
{
    use HasFactory, CreateDefaults, SoftDeletes;

    protected $fillable = [
        'user_id',
        'title'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public static function getPaymentRecurrencesWithExpensesCount($userID)
    {
        $user = User::find($userID);
        return $user->paymentRecurrences()->withCount('expenses')->orderBy('title')->get();
    }

    public static function createDefaultPaymentRecurrences($userID)
    {
        $paymentRecurrences = CreateDefaults::getItems('payment-recurrences-example', $userID);
        return PaymentRecurrence::insert($paymentRecurrences);
    }
}
