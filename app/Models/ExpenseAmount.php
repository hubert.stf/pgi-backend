<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenseAmount extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'expense_id',
        'reference_month',
        'due_date',
        'amount',
        'paid_out_at',
        'is_payment_confirmed'
    ];

    public function expense()
    {
        return $this->belongsTo(Expense::class);
    }

    public function expenseAmountDiscounts()
    {
        return $this->hasMany(ExpenseAmountDiscount::class);
    }

    public static function getAllExpenseAmountsByExpenseId($expenseId)
    {
        return ExpenseAmount::where('expense_id', '=', $expenseId)
            ->orderBy('reference_month')
            ->orderBy('due_date')
            ->get();
    }
}
