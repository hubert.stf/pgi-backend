<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CurrentBalance extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'period_id',
        'title',
        'description',
        'amount'
    ];

    public function period()
    {
        return $this->belongsTo(Period::class);
    }
}
