<?php

namespace App\Models;

use App\Traits\CreateDefaults;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    use HasFactory, CreateDefaults, SoftDeletes;

    protected $fillable = [
        'user_id',
        'title',
        'color'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public static function getPaymentMethodsWithExpensesCount($userID)
    {
        $user = User::find($userID);
        return $user->paymentMethods()->withCount('expenses')->orderBy('title')->get();
    }

    public static function createDefaultPaymentMethods($userID)
    {
        $paymentMethods = CreateDefaults::getItems('payment-methods-example', $userID);
        return PaymentMethod::insert($paymentMethods);
    }
}
