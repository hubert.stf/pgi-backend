<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Income extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'period_id',
        'title'
    ];

    public function period()
    {
        return $this->belongsTo(Period::class);
    }

    public function incomeAmounts()
    {
        return $this->hasMany(IncomeAmount::class);
    }

    public static function getAllIncomesByPeriodId($periodId)
    {
        return Period::findOrFail($periodId)
            ->incomes()
            ->withSum('incomeAmounts', 'amount')
            ->orderBy('title')
            ->get();
    }

    public static function getIncomeWithAmountsSum($incomeId)
    {
        return Income::where('id', '=', $incomeId)
            ->withSum('incomeAmounts', 'amount')
            ->first();
    }
}
