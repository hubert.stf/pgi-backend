<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'period_id',
        'category_id',
        'payment_recurrence_id',
        'payment_status_id',
        'payment_method_id',
        'company_nickname',
        'description',
        'purchase_date',
        'number_of_installments',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function paymentRecurrence()
    {
        return $this->belongsTo(PaymentRecurrence::class);
    }

    public function paymentStatus()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    public function period()
    {
        return $this->belongsTo(Period::class);
    }

    public function expenseAmounts()
    {
        return $this->hasMany(ExpenseAmount::class);
    }

    public static function getAllExpensesWithAmountsByPeriodId($periodId)
    {
        return Period::findOrFail($periodId)
            ->expenses()
            ->with('category', 'paymentMethod', 'paymentRecurrence', 'paymentStatus', 'expenseAmounts')
            ->withSum('expenseAmounts', 'amount')
            ->get();
    }

    public static function getExpenseWithAmountsById($expenseId)
    {
        return Expense::where('id', '=', $expenseId)
            ->with('category', 'paymentMethod', 'paymentRecurrence', 'paymentStatus', 'expenseAmounts')
            ->withSum('expenseAmounts', 'amount')
            ->first();
    }
}
