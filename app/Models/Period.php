<?php

namespace App\Models;

use App\Traits\CreateDefaults;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Period extends Model
{
    use HasFactory, SoftDeletes, CreateDefaults;

    protected $fillable = [
        'user_id',
        'title',
        'start_date',
        'end_date',
        'is_default',
        'is_to_display'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currentBalances()
    {
        return $this->hasMany(CurrentBalance::class);
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public function incomes()
    {
        return $this->hasMany(Income::class);
    }

    public static function createDefaultPeriods($userID)
    {
        $periods = CreateDefaults::getItemsFromStorage('periods-example', $userID);
        $periods = CreateDefaults::addUserId($periods, $userID);
        $periods = CreateDefaults::addTimestamps($periods);
        $periods = CreateDefaults::updateDates($periods);
        return Period::insert($periods);
    }

    public static function unsetCurrentDefaultPeriod()
    {
        Period::where('is_default', true)->update(['is_default' => false]);
    }
}
