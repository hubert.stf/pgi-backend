<?php

namespace App\Models;

use App\Traits\CreateDefaults;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, CreateDefaults, SoftDeletes;

    protected $fillable = [
        'user_id',
        'title'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public static function getCategoriesWithExpensesCount($userID)
    {
        $user = User::find($userID);
        return $user->categories()->withCount('expenses')->orderBy('title')->get();
    }

    public static function createDefaultCategories($userID)
    {
        $categories = CreateDefaults::getItems('categories-example', $userID);
        return Category::insert($categories);
    }
}
