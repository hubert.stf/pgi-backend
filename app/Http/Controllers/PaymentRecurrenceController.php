<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaymentRecurrenceRequest;
use App\Models\PaymentRecurrence;
use Illuminate\Http\Response;

class PaymentRecurrenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return PaymentRecurrence::getPaymentRecurrencesWithExpensesCount(auth()->id());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePaymentRecurrenceRequest $request
     * @return Response
     */
    public function store(StorePaymentRecurrenceRequest $request)
    {
        return PaymentRecurrence::create([
            'user_id' => $request->user()->id,
            'title' => $request->title
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StorePaymentRecurrenceRequest $request
     * @param int $id
     * @return Response
     */
    public function update(StorePaymentRecurrenceRequest $request, $id)
    {
        $paymentRecurrence = PaymentRecurrence::find($id);
        if ($paymentRecurrence->user_id === $request->user()->id) {
            $paymentRecurrence->title = $request->title;
        }
        return $paymentRecurrence->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $authUserID = auth()->id();
        $paymentRecurrenceToDelete = PaymentRecurrence::find($id);
        if ($paymentRecurrenceToDelete->user_id === $authUserID) {
            return response(PaymentRecurrence::destroy($id));
        }
    }
}
