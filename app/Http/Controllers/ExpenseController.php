<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteExpenseRequest;
use App\Http\Requests\StoreExpenseRequest;
use App\Http\Requests\UpdateExpenseRequest;
use App\Models\Expense;
use App\Models\ExpenseAmount;
use App\Models\Period;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ExpenseController extends Controller
{
    private function isUserAuthorizedToMakeTheRequest($periodId)
    {
        $periodInstance = Period::findOrFail($periodId);
        return $periodInstance->user_id === auth()->id();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if ($this->isUserAuthorizedToMakeTheRequest($request->period_id)) {
            return Expense::getAllExpensesWithAmountsByPeriodId($request->period_id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreExpenseRequest $request
     * @return Response
     */
    public function store(StoreExpenseRequest $request)
    {
        return DB::transaction(function () use ($request) {
            $expense = Expense::create([
                'user_id' => auth()->id(),
                'period_id' => $request->period_id,
                'category_id' => $request->category_id,
                'payment_recurrence_id' => $request->payment_recurrence_id,
                'payment_status_id' => $request->payment_status_id,
                'payment_method_id' => $request->payment_method_id,
                'company_nickname' => $request->company_nickname,
                'description' => $request->description,
                'purchase_date' => $request->purchase_date,
                'number_of_installments' => $request->number_of_installments,
            ]);

            if ($expense->id && isset($request->expense_amounts)) {
                foreach ($request->expense_amounts as $expenseAmount) {
                    ExpenseAmount::create([
                        'user_id' => auth()->id(),
                        'expense_id' => $expense->id,
                        'reference_month' => $expenseAmount['reference_month'],
                        'due_date' => $expenseAmount['due_date'],
                        'amount' => $expenseAmount['amount'],
                        'paid_out_at' => $expenseAmount['paid_out_at'],
                    ]);
                }
            }

            return $expense;
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Expense::getAllExpensesWithAmountsById($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateExpenseRequest $request, $id)
    {
        return DB::transaction(function () use ($request, $id) {
            $expenseInstance = Expense::findOrFail($id);
            $expenseInstance->purchase_date = $request->purchase_date;
            $expenseInstance->category_id = $request->category_id;
            $expenseInstance->company_nickname = $request->company_nickname;
            $expenseInstance->description = $request->description;
            $expenseInstance->payment_method_id = $request->payment_method_id;
            $expenseInstance->payment_status_id = $request->payment_status_id;
            $expenseInstance->payment_recurrence_id = $request->payment_recurrences_id;
            $expenseInstance->number_of_installments = $request->number_of_installments;
            $expenseInstance->save();

            ExpenseAmount::where('expense_id', '=', $expenseInstance->id)->delete();

            foreach ($request->expense_amounts as $expenseAmount) {
                ExpenseAmount::create([
                    'user_id' => auth()->id(),
                    'expense_id' => $expenseInstance->id,
                    'reference_month' => $expenseAmount['reference_month'],
                    'due_date' => $expenseAmount['due_date'],
                    'amount' => $expenseAmount['amount'],
                    'paid_out_at' => $expenseAmount['paid_out_at'],
                ]);
            }

            return Expense::getExpenseWithAmountsById($expenseInstance->id);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(DeleteExpenseRequest $request, $id)
    {
        return Response(Expense::destroy($id));
    }
}
