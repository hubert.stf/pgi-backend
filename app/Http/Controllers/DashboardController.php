<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use App\Models\ExpenseAmount;
use App\Models\Income;
use App\Models\IncomeAmount;
use App\Models\Period;
use Carbon\Carbon;

class DashboardController extends Controller
{
    private $period;
    private $startDate;
    private $endDate;
    private $incomeIds;
    private $expenseIds;
    private $chartData = [];

    private function getIncomeAmountsSumByPeriodId($periodId): int
    {
        $incomes = Income::where('period_id', '=', $periodId)
            ->withSum('incomeAmounts', 'amount')
            ->get();

        $incomesSum = 0;
        foreach ($incomes as $income) {
            $incomesSum += $income->income_amounts_sum_amount;
        }

        return $incomesSum;
    }

    private function getExpenseAmountsSumByPeriodId($periodId): int
    {
        $expenses = Expense::where('period_id', '=', $periodId)
            ->withSum('expenseAmounts', 'amount')
            ->get();

        $expenseSum = 0;
        foreach ($expenses as $expense) {
            $expenseSum += $expense->expense_amounts_sum_amount;
        }

        return $expenseSum;
    }

    public function getStats(Period $period)
    {
        if ($period->user_id !== auth()->id()) {
            return null;
        }

        $incomeAmountsSum = $this->getIncomeAmountsSumByPeriodId($period->id);
        $expenseAmountsSum = $this->getExpenseAmountsSumByPeriodId($period->id);
        $differenceBetweenIncomesSumAndExpensesSum = $incomeAmountsSum - $expenseAmountsSum;

        return [
            'incomesSum' => $incomeAmountsSum,
            'expensesSum' => $expenseAmountsSum,
            'difference' => $differenceBetweenIncomesSumAndExpensesSum,
        ];
    }

    private function setPeriod($period)
    {
        $this->period = $period;
    }

    private function setStartDateWithoutDay()
    {
        $startDateArray = Carbon::parse($this->period->start_date)->toArray();
        $this->startDate = Carbon::create("{$startDateArray['year']}-{$startDateArray['month']}");
    }

    private function setEndDateWithoutDay()
    {
        $endDateArray = Carbon::parse($this->period->end_date)->toArray();
        $this->endDate = Carbon::create("{$endDateArray['year']}-{$endDateArray['month']}");
    }

    private function getReferenceMonthFromDate($date)
    {
        return substr($date->toDateString(), 0, 7);
    }

    private function setIncomeIdsByPeriod()
    {
        $this->incomeIds = Income::where('period_id', '=', $this->period->id)->pluck('id');
    }

    private function setExpenseIdsByPeriod()
    {
        $this->expenseIds = Expense::where('period_id', '=', $this->period->id)->pluck('id');
    }

    private function getIncomeSumByReferenceMonth($index)
    {
        return IncomeAmount::whereIn('income_id', $this->incomeIds)
            ->where('reference_month', '=', $this->chartData[$index]['referenceMonth'])
            ->sum('amount');
    }

    private function getExpenseSumByReferenceMonth($index)
    {
         return ExpenseAmount::whereIn('expense_id', $this->expenseIds)
            ->where('reference_month', '=', $this->chartData[$index]['referenceMonth'])
            ->sum('amount');
    }

    private function getDifferenceBetweenIncomesAndExpenses($index)
    {
        return $this->chartData[$index]['incomesSum'] - $this->chartData[$index]['expensesSum'];
    }

    private function getAccruedDifference($index)
    {
        if ($index === 0) {
            return $this->chartData[$index]['difference'];
        } else {
            return $this->chartData[$index-1]['difference'] + $this->chartData[$index]['difference'];
        }
    }

    public function getChartData(Period $period): array
    {
        $this->setPeriod($period);
        $this->setStartDateWithoutDay();
        $this->setEndDateWithoutDay();
        $this->setIncomeIdsByPeriod();
        $this->setExpenseIdsByPeriod();

        // control variables
        $index = 0;
        $currentDate = $this->startDate;

        while ($currentDate->lessThanOrEqualTo($this->endDate)) {
            $this->chartData[$index]['referenceMonth'] = $this->getReferenceMonthFromDate($currentDate);
            $this->chartData[$index]['incomesSum'] = $this->getIncomeSumByReferenceMonth($index);
            $this->chartData[$index]['expensesSum'] = $this->getExpenseSumByReferenceMonth($index);
            $this->chartData[$index]['difference'] = $this->getDifferenceBetweenIncomesAndExpenses($index);
            $this->chartData[$index]['accrued'] = $this->getAccruedDifference($index);

            $index++;
            $currentDate->addMonth();
        }

        return $this->chartData;
    }
}
