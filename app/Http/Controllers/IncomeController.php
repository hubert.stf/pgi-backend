<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteIncomeRequest;
use App\Http\Requests\StoreIncomeRequest;
use App\Http\Requests\UpdateIncomeRequest;
use App\Models\Income;
use App\Models\Period;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IncomeController extends Controller
{
    private function isUserAuthorizedToMakeTheRequest($periodId)
    {
        $periodInstance = Period::findOrFail($periodId);
        return $periodInstance->user_id === auth()->id();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if ($this->isUserAuthorizedToMakeTheRequest($request->period_id)) {
            return Income::getAllIncomesByPeriodId($request->period_id);
        }
    }

    public function show(Income $income)
    {
        if ($this->isUserAuthorizedToMakeTheRequest($income->period_id)) {
            return Income::getIncomeWithAmountsSum($income->id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreIncomeRequest $request
     * @return Response
     */
    public function store(StoreIncomeRequest $request)
    {
        return Income::create([
            'user_id' => auth()->id(),
            'title' => $request->title,
            'period_id' => $request->period_id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateIncomeRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateIncomeRequest $request, $id)
    {
        $incomeInstance = Income::findOrFail($id);
        $incomeInstance->title = $request->title;
        return $incomeInstance->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(DeleteIncomeRequest $request, $id)
    {
        return Response(Income::destroy($id));
    }
}
