<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Category::getCategoriesWithExpensesCount(auth()->id());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategoryRequest $request
     * @return Response
     */
    public function store(StoreCategoryRequest $request)
    {
        return Category::create([
            'user_id' => $request->user()->id,
            'title' => $request->title
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreCategoryRequest $request
     * @param int $id
     * @return Response
     */
    public function update(StoreCategoryRequest $request, $id)
    {
        $category = Category::find($id);
        if ($category->user_id === $request->user()->id) {
            $category->title = $request->title;
        }
        return $category->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $authUserID = auth()->id();
        $categoryToDelete = Category::find($id);
        if ($categoryToDelete->user_id === $authUserID) {
            return response(Category::destroy($id));
        }
    }
}
