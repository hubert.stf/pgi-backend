<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePeriodsRequest;
use App\Http\Requests\UpdatePeriodsRequest;
use App\Models\Period;
use App\Models\User;
use Illuminate\Http\Response;

class PeriodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = User::find(auth()->id());
        return $user->periods()
            ->orderBy('is_default', 'desc')
            ->orderBy('is_to_display', 'desc')
            ->orderBy('start_date')
            ->orderBy('end_date')
            ->orderBy('title')
            ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePeriodsRequest $request
     * @return Response
     */
    public function store(StorePeriodsRequest $request)
    {
        return Period::create([
            'user_id' => $request->user()->id,
            'title' => $request->title,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePeriodsRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdatePeriodsRequest $request, $id)
    {
        $period = Period::findOrFail($id);

        if (isset($request->title)) {
            $period->title = $request->title;
        }
        if (isset($request->start_date)) {
            $period->start_date = $request->start_date;
        }
        if (isset($request->end_date)) {
            $period->end_date = $request->end_date;
        }
        if (isset($request->is_default)) {
            Period::unsetCurrentDefaultPeriod();
            $period->is_default = $request->is_default;
        }
        if (isset($request->is_to_display)) {
            $period->is_to_display = $request->is_to_display;
        }

        return $period->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $authUserID = auth()->id();
        $periodToDelete = Period::find($id);
        if ($periodToDelete->user_id === $authUserID) {
            return response(Period::destroy($id));
        }
    }
}
