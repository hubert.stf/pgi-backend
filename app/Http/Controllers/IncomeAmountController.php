<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteIncomeAmountRequest;
use App\Http\Requests\StoreIncomeAmountRequest;
use App\Http\Requests\UpdateIncomeAmountRequest;
use App\Models\Income;
use App\Models\IncomeAmount;
use Illuminate\Http\Response;

class IncomeAmountController extends Controller
{
    public function getAllByIncomeId(Income $income)
    {
        if ($income->user_id === auth()->id()) {
            return IncomeAmount::getAllIncomeAmountsByIncomeId($income->id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreIncomeAmountRequest $request
     * @return Response
     */
    public function store(StoreIncomeAmountRequest $request)
    {
        return IncomeAmount::create([
            'user_id' => auth()->id(),
            'income_id' => $request->income_id,
            'reference_month' => $request->reference_month,
            'amount' => $request->amount,
            'due_date' => $request->due_date,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateIncomeAmountRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateIncomeAmountRequest $request, $id)
    {
        $incomeAmountInstance = IncomeAmount::findOrFail($id);

        if (isset($request->reference_month)) {
            $incomeAmountInstance->reference_month = $request->reference_month;
        }
        if (isset($request->amount)) {
            $incomeAmountInstance->amount = $request->amount;
        }
        if (isset($request->due_date)) {
            $incomeAmountInstance->due_date = $request->due_date;
        }
        if (isset($request->is_payment_confirmed)) {
            $incomeAmountInstance->is_payment_confirmed = $request->is_payment_confirmed;
        }
        if (isset($request->payment_received_at) || is_null($request->payment_received_at)) {
            $incomeAmountInstance->payment_received_at = $request->payment_received_at;
        }

        $wasSaved = $incomeAmountInstance->save();
        if ($wasSaved) {
            return $incomeAmountInstance;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(DeleteIncomeAmountRequest $request, $id)
    {
        $incomeAmountInstance = IncomeAmount::findOrFail($id);
        $wasDeleted = IncomeAmount::destroy($id);
        if ($wasDeleted) {
            return Response($incomeAmountInstance);
        }
    }
}
