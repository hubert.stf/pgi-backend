<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaymentStatusRequest;
use App\Http\Requests\UpdatePaymentStatusRequest;
use App\Models\PaymentStatus;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return PaymentStatus::getPaymentStatusesWithExpensesCount(auth()->id());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(StorePaymentStatusRequest $request)
    {
        return PaymentStatus::create([
            'user_id' => $request->user()->id,
            'title' => $request->title,
            'color' => $request->color,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePaymentStatusRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdatePaymentStatusRequest $request, $id)
    {
        $paymentStatus = PaymentStatus::find($id);
        if ($paymentStatus->user_id === $request->user()->id) {
            if (isset($request->title)) {
                $paymentStatus->title = $request->title;
            }
            if (isset($request->color)) {
                $paymentStatus->color = $request->color;
            }
        }
        return $paymentStatus->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $authUserID = auth()->id();
        $paymentStatusToDelete = PaymentStatus::find($id);
        if ($paymentStatusToDelete->user_id === $authUserID) {
            return response(PaymentStatus::destroy($id));
        }
    }
}
