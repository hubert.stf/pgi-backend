<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $request->user();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateFullName (Request $request)
    {
        $validated = $request->validate([
            'first_name' => ['required', 'string', 'max:32'],
            'last_name' => ['required', 'string', 'max:64'],
        ]);

        if ($validated) {
            $user = $request->user();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->save();
        }
    }

    public function updateLanguage (Request $request)
    {
        $validated = $request->validate([
            'language' => ['required', Rule::in(['pt-BR', 'en-US'])],
        ]);

        if ($validated) {
            $user = $request->user();
            $user->language = $request->language;
            $user->save();
        }
    }

    public function checkIfTheEmailHasAlreadyHasBeenTaken (Request $request): array
    {
        return $request->validate([
            'email' => ['email', Rule::unique(User::class)],
        ]);
    }
}
