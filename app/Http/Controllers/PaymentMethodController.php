<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaymentMethodRequest;
use App\Http\Requests\UpdatePaymentMethodRequest;
use App\Models\PaymentMethod;
use Illuminate\Http\Response;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return PaymentMethod::getPaymentMethodsWithExpensesCount(auth()->id());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePaymentMethodRequest $request
     * @return Response
     */
    public function store(StorePaymentMethodRequest $request)
    {
        return PaymentMethod::create([
            'user_id' => $request->user()->id,
            'title' => $request->title,
            'color' => $request->color,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePaymentMethodRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdatePaymentMethodRequest $request, $id)
    {
        $paymentMethod = PaymentMethod::find($id);
        if ($paymentMethod->user_id === $request->user()->id) {
            if (isset($request->title)) {
                $paymentMethod->title = $request->title;
            }
            if (isset($request->color)) {
                $paymentMethod->color = $request->color;
            }
        }
        return $paymentMethod->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $authUserID = auth()->id();
        $paymentMethodToDelete = PaymentMethod::find($id);
        if ($paymentMethodToDelete->user_id === $authUserID) {
            return response(PaymentMethod::destroy($id));
        }
    }
}
