<?php

namespace App\Http\Requests;

use App\Models\Income;
use App\Models\IncomeAmount;
use App\Models\Period;
use Illuminate\Foundation\Http\FormRequest;

class UpdateIncomeAmountRequest extends FormRequest
{
    private $incomeAmountInstance;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $incomeAmountId = $this->route('income_amount');
        $this->incomeAmountInstance = IncomeAmount::findOrFail($incomeAmountId);

        return $this->incomeAmountInstance->user_id === auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $incomeInstance = Income::findOrFail($this->incomeAmountInstance->income_id);
        $periodInstance = Period::findOrFail($incomeInstance->period_id);

        $endDate = $periodInstance->end_date;
        $startDate = $periodInstance->start_date;

        return [
            'reference_month' => [
                'sometimes',
                'min:7',
                'max:7',
            ],
            'amount' => [
                'sometimes',
                'numeric',
                'between:0,99999999999.9999',
            ],
            'due_date' => [
                'sometimes',
                'date',
                "before_or_equal:$endDate",
                "after_or_equal:$startDate",
            ],
            'is_payment_confirmed' => [
                'sometimes',
                'boolean',
            ],
            'payment_received_at' => [
                'sometimes',
                'nullable',
                'date',
                "before_or_equal:$endDate",
                "after_or_equal:$startDate",
            ]
        ];
    }
}
