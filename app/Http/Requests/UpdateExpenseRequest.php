<?php

namespace App\Http\Requests;

use App\Models\Expense;
use App\Models\Period;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdateExpenseRequest extends FormRequest
{
    private $periodInstance;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $expenseInstance = Expense::findOrFail($request->id);
        $this->periodInstance = Period::findOrFail($expenseInstance->period_id);

        return $this->periodInstance->user_id === auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $startDate = $this->periodInstance->start_date;
        $endDate = $this->periodInstance->end_date;

        return [
            'category_id' => [
                'nullable',
                'exists:categories,id',
            ],
            'payment_recurrence_id' => [
                'nullable',
                'exists:payment_recurrences,id',
            ],
            'payment_status_id' => [
                'nullable',
                'exists:payment_statuses,id',
            ],
            'payment_method_id' => [
                'nullable',
                'exists:payment_methods,id',
            ],
            'company_nickname' => [
                'required',
                'min:2',
                'max:64',
            ],
            'description' => [
                'nullable',
                'max:2048',
            ],
            'purchase_date' => [
                'nullable',
                'date',
                "before_or_equal:$endDate",
                "after_or_equal:$startDate",
            ],
            'number_of_installments' => [
                'required',
                'numeric',
                'min:1',
                'max:85',
            ],
            'expense_amounts' => [
                'required',
                'array',
                'min:1'
            ],
            'expense_amounts.*.amount' => [
                'required',
                'numeric',
                'between:0,99999999999.9999',
            ],
            'expense_amounts.*.due_date' => [
                'nullable',
                'date',
                "before_or_equal:$endDate",
                "after_or_equal:$startDate",
            ],
            'expense_amounts.*.paid_out_at' => [
                'nullable',
                'date',
                "before_or_equal:$endDate",
                "after_or_equal:$startDate",
            ],
            'expense_amounts.*.reference_month' => [
                'required',
                'min:7',
                'max:7',
            ],
        ];
    }
}
