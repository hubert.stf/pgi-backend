<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $authUserID = auth()->id();

        return [
            'title' => [
                'required',
                'min:3',
                'max:64',
                Rule::unique('categories', 'title')->where(function ($query) use ($authUserID) {
                    return $query
                        ->where('user_id', $authUserID)
                        ->where('deleted_at', null);
                })
            ]
        ];
    }
}
