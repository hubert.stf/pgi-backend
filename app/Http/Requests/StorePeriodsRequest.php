<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StorePeriodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'title' => [
                'required',
                'min:3',
                'max:64',
                Rule::unique('periods', 'title')->where(function ($query) use ($request) {
                    return $query
                        ->where('user_id', $request->user()->id)
                        ->where('start_date', $request->start_date)
                        ->where('end_date', $request->end_date)
                        ->where('deleted_at', null);
                })
            ],
            'start_date' => [
                'required',
                'date'
            ],
            'end_date' => [
                'required',
                'date',
                'gte:start_date'
            ]
        ];
    }
}
