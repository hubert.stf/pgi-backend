<?php

namespace App\Http\Requests;

use App\Models\IncomeAmount;
use Illuminate\Foundation\Http\FormRequest;

class DeleteIncomeAmountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $incomeAmountId = $this->route('income_amount');
        $incomeAmountInstance = IncomeAmount::findOrFail($incomeAmountId);

        return $incomeAmountInstance->user_id === auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
