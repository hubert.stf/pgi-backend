<?php

namespace App\Http\Requests;

use App\Models\Period;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StoreIncomeRequest extends FormRequest
{
    private $periodInstance;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $this->periodInstance = Period::findOrFail($request->period_id);

        return $this->periodInstance->user_id === auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $periodID = $this->periodInstance->id;

        return [
            'period_id' => [
                'required',
                'exists:periods,id'
            ],
            'title' => [
                'required',
                'min:2',
                'max:64',
                Rule::unique('incomes', 'title')->where(function ($query) use ($periodID) {
                    return $query
                        ->where('period_id', $periodID)
                        ->where('deleted_at', null);
                })
            ],
        ];
    }
}
