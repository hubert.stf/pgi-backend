<?php

namespace App\Http\Requests;

use App\Models\Period;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePeriodsRequest extends FormRequest
{
    private $periodInstance;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $periodId = $this->route('period');
        $this->periodInstance = Period::find($periodId);

        return $this->periodInstance->user_id === auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $periodInstance = $this->periodInstance;

        return [
            'title' => [
                'sometimes',
                'min:3',
                'max:64',
                Rule::unique('periods', 'title')->where(function ($query) use ($periodInstance) {
                    return $query
                        ->where('id', '!=', $periodInstance->id)
                        ->where('user_id', $periodInstance->user_id)
                        ->where('start_date', $periodInstance->start_date)
                        ->where('end_date', $periodInstance->end_date)
                        ->where('deleted_at', null);
                })
            ],
            'start_date' => [
                'sometimes',
                'date',
                'before_or_equal:end_date'
            ],
            'end_date' => [
                'sometimes',
                'date',
                'after_or_equal:start_date'
            ],
            'is_default' => [
                'sometimes',
                'boolean'
            ],
            'is_to_display' => [
                'sometimes',
                'boolean'
            ]
        ];
    }
}
