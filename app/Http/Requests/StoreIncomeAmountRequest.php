<?php

namespace App\Http\Requests;

use App\Models\Income;
use App\Models\Period;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreIncomeAmountRequest extends FormRequest
{
    private $incomeInstance;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $this->incomeInstance = Income::findOrFail($request->income_id);

        return $this->incomeInstance->user_id === auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $periodInstance = Period::findOrFail($this->incomeInstance->period_id);

        $endDate = $periodInstance->end_date;
        $startDate = $periodInstance->start_date;

        return [
            'income_id' => [
                'required',
                'exists:incomes,id',
            ],
            'reference_month' => [
                'required',
                'min:7',
                'max:7',
            ],
            'amount' => [
                'required',
                'numeric',
                'between:0,99999999999.9999',
            ],
            'due_date' => [
                'nullable',
                'date',
                "before_or_equal:$endDate",
                "after_or_equal:$startDate",
            ],
        ];
    }
}
