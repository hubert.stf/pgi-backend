<?php

namespace App\Http\Requests;

use App\Models\Expense;
use App\Models\Period;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreExpenseAmountRequest extends FormRequest
{
    private $expenseInstance;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $this->expenseInstance = Expense::findOrFail($request->expense_id);

        return $this->expenseInstance->user_id === auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $periodInstance = Period::findOrFail($this->expenseInstance->period_id);

        $endDate = $periodInstance->end_date;
        $startDate = $periodInstance->start_date;

        return [
            'expense_id' => [
                'required',
                'exists:expenses,id',
            ],
            'reference_month' => [
                'required',
                'min:7',
                'max:7',
            ],
            'amount' => [
                'required',
                'numeric',
                'between:0,99999999999.9999',
            ],
            'due_date' => [
                'date',
                "before_or_equal:$endDate",
                "after_or_equal:$startDate",
            ],
            'paid_out_at' => [
                'date',
                "before_or_equal:$endDate",
                "after_or_equal:$startDate",
            ],
        ];
    }
}
