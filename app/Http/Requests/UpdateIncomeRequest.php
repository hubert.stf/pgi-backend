<?php

namespace App\Http\Requests;

use App\Models\Income;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateIncomeRequest extends FormRequest
{
    private $incomeInstance;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $incomeId = $this->route('income');
        $this->incomeInstance = Income::findOrFail($incomeId);

        return $this->incomeInstance->user_id === auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $incomeId = $this->incomeInstance->id;
        $periodId = $this->incomeInstance->period_id;

        return [
            'period_id' => [
                'sometimes',
                'required',
                'exists:periods,id'
            ],
            'title' => [
                'required',
                'min:2',
                'max:64',
                Rule::unique('incomes', 'title')->where(function ($query) use ($incomeId, $periodId) {
                    return $query
                        ->where('id', '!=', $incomeId)
                        ->where('period_id', $periodId)
                        ->where('deleted_at', null);
                })
            ],
        ];
    }
}
