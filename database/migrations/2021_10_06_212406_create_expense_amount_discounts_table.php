<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseAmountDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_amount_discounts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('expense_amount_id');
            $table->foreignId('user_id');
            $table->text('description')->nullable();
            $table->decimal('amount', 15, 4);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('expense_amount_id')->references('id')->on('expense_amounts')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_amount_discounts');
    }
}
