<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomeAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('income_amounts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('income_id');
            $table->foreignId('user_id');
            $table->char('reference_month', '7');
            $table->date('due_date')->nullable();
            $table->decimal('amount', 15, 4);
            $table->date('payment_received_at')->nullable();
            $table->boolean('is_payment_confirmed')->default(true);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('income_id')->references('id')->on('incomes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('income_amounts');
    }
}
