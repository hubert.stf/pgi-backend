<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periods', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('title', 64);
            $table->date('start_date');
            $table->date('end_date');
            $table->boolean('is_default')->default(false);
            $table->boolean('is_to_display')->default(true);
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['user_id', 'title', 'start_date', 'end_date', 'deleted_at']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periods');
    }
}
