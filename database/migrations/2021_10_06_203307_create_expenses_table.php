<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('period_id');
            $table->foreignId('category_id')->nullable();
            $table->foreignId('payment_recurrence_id')->nullable();
            $table->foreignId('payment_status_id')->nullable();
            $table->foreignId('payment_method_id')->nullable();
            $table->string('company_nickname', 64);
            $table->foreignId('user_id');
            $table->text('description')->nullable();
            $table->date('purchase_date')->nullable();
            $table->tinyText('number_of_installments');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set NULL')->onUpdate('cascade');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('set NULL')->onUpdate('cascade');
            $table->foreign('payment_recurrence_id')->references('id')->on('payment_recurrences')->onDelete('set NULL')->onUpdate('cascade');
            $table->foreign('payment_status_id')->references('id')->on('payment_statuses')->onDelete('set NULL')->onUpdate('cascade');
            $table->foreign('period_id')->references('id')->on('periods')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
