<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ExpenseAmountController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\IncomeAmountController;
use App\Http\Controllers\IncomeController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\PaymentRecurrenceController;
use App\Http\Controllers\PaymentStatusController;
use App\Http\Controllers\PeriodsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Features;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
| PUBLIC ROUTES
*/
if (Features::enabled(Features::registration())) {
    Route::post('/register', [RegisteredUserController::class, 'store']);
}

$limiter = config('fortify.limiters.login');
Route::post('/login', [AuthenticatedSessionController::class, 'store'])
    ->middleware($limiter ? 'throttle:'.$limiter : null);

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
    ->name('logout');

Route::post('/user/email/verify', [UserController::class, 'checkIfTheEmailHasAlreadyHasBeenTaken'])
    ->name('user.unique-email');
// PUBLIC ROUTES - END

Route::middleware('auth:sanctum')
    ->put('/user/update-fullname', [UserController::class, 'updateFullName'])
    ->name('user.update-fullname');
Route::middleware('auth:sanctum')
    ->put('/user/update-language', [UserController::class, 'updateLanguage'])
    ->name('user.update-language');
Route::middleware('auth:sanctum')
    ->resource('user', UserController::class);

Route::middleware('auth:sanctum')
    ->resource('categories', CategoryController::class);

Route::middleware('auth:sanctum')
    ->resource('payment-methods', PaymentMethodController::class);

Route::middleware('auth:sanctum')
    ->resource('payment-recurrences', PaymentRecurrenceController::class);

Route::middleware('auth:sanctum')
    ->resource('payment-statuses', PaymentStatusController::class);

Route::middleware('auth:sanctum')
    ->resource('periods', PeriodsController::class);

Route::middleware('auth:sanctum')
    ->resource('incomes', IncomeController::class);

Route::middleware('auth:sanctum')
    ->get('/income-amounts/{income}', [IncomeAmountController::class, 'getAllByIncomeId'])
    ->name('income-amount.getAllByIncomeId');
Route::middleware('auth:sanctum')
    ->resource('income-amounts', IncomeAmountController::class);

Route::middleware('auth:sanctum')
    ->resource('expenses', ExpenseController::class);

Route::middleware('auth:sanctum')
    ->get('/expense-amounts/{expense}', [ExpenseAmountController::class, 'getAllByExpenseId'])
    ->name('expense-amount.getAllByExpenseId');
Route::middleware('auth:sanctum')
    ->resource('expense-amounts', ExpenseAmountController::class);

Route::middleware('auth:sanctum')
    ->get('/dashboard/stats/{period}', [DashboardController::class, 'getStats'])
    ->name('dashboard.getStats');
Route::middleware('auth:sanctum')
    ->get('/dashboard/chart-data/{period}', [DashboardController::class, 'getChartData'])
    ->name('dashboard.getChartData');
Route::middleware('auth:sanctum')
    ->resource('dashboard', DashboardController::class);
